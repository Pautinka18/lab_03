<?php
require_once ("connect.php");
    $sql = "SELECT * FROM users";
    $result = mysqli_query($conn, $sql);
    $users = mysqli_fetch_all($result, MYSQLI_ASSOC);

    function delete_users($conn, $id)
    {
        $sql = $conn->prepare("DELETE FROM users WHERE id = ?");
        $sql->bind_param('i', $id);
        $sql->execute();
        header('Location: users.php');

    }

    function add_users($conn, $user2)
    {
        $sql = $conn->prepare("INSERT INTO `users` (`id`, `login`, `password`, `name`, `surname`, `language`, `role`) VALUES (NULL, ?, ?, ?, ?, ?, ?)");
        $sql->bind_param("ssssss", $user2['login'], $user2['password'], $user2['name'], $user2['surname'], $user2['language'], $user2['role']);
        $sql->execute();
        if ($_SESSION['id']) {
            header('Location: users.php');
        }
    }

    function edit_users($conn, $user2)
    {
        $sql = $conn->prepare("UPDATE `users` SET `name`= ?, `surname`= ?, `login`= ?, `password`= ?, `language`= ?, `role`= ? WHERE `users`.`id`= ?");
        $sql->bind_param("ssssssi", $user2['name'], $user2['surname'], $user2['login'], $user2['password'], $user2['language'], $user2['role'], $user2['id']);
        $sql->execute();
        if($_SESSION['id']){
        header('Location: users.php');
        }
    }

    class Users
    {
        /**
         * User constructor.
         * @param $name
         * @param $surname
         * @param $role
         * @param $login
         * @param $password
         */
        public function __construct($temp_user)
        {
            $this->name = $temp_user['name'];
            $this->surname = $temp_user['surname'];
            $this->role = $temp_user['role'];
            $this->lang= $temp_user['language'];
            $_SESSION['id']=$temp_user['id'];
        }
        public function getLang(){
            return $this->lang;
        }
        public function isAdmin(){
            return $this->role=='admin';
        }
        public function isManager(){
            return $this->role=='manager';
        }
        public function isClient(){
            return $this->role=='client';
        }

    }

    class admin extends Users
    {
        public function gotoNext()
        {
            header("Location: admin.php");
        }
    }

    class client extends Users
    {
        public function gotoNext()
        {
            header("Location: client.php");
        }
    }

    class manager extends Users
    {
        public function gotoNext()
        {
            header("Location: manager.php");
        }
    }

    $roles = [
        'admin' => admin::class,
        'manager' => manager::class,
        'client' => client::class,
    ];

    $translate = [
        'ru' => ['Hello' => 'Здравствуйте', 'You' => 'Вы', 'can' => 'можете', 'change' => 'изменить', 'language' => 'язык', 'below' => 'ниже', 'Personal cabinet' => 'Личный кабинет', 'Control panel' => 'Панель управления', 'Name' => 'Имя', 'Surname' => 'Фамилия', 'Role' => 'Роль', 'You can change your role below :' => 'Вы можете сменить вашу роль на сайте ниже :', 'User edit' => 'Редактирование пользователей', 'Registration' => 'Регистрация', 'Hello Guest' => 'Привет Гость', 'Enter login and password for authorization' => 'Введите логин и пароль для авторизации', 'Login' => 'Логин', 'Password' => 'Пароль', 'Language' => 'Язык', 'Search' => 'Поиск', 'Sign out' => 'Выход из аккаунта', 'Back' => 'Вернутся обратно', 'User table' => 'Таблица пользователей', 'Edit' => 'Редактировать', 'Delete' => 'Удалить', 'Add new user' => 'Добавить нового пользователя', 'Change user data' => 'Изменения данных о пользователе', 'Account creation' => 'Создание аккаунта', 'Enter this data to search' => '
Введите эти данные для поиска', 'Searched user' => 'Искомый пользователь'],
        'ua' => ['Hello' => 'Здравствуйте', 'You' => 'Ви', 'can' => 'можете', 'change' => 'змінити', 'language' => 'мову', 'below' => 'нижче', 'Personal cabinet' => 'Особистий кабінет', 'Control panel' => 'Панель управління', 'Name' => 'Ім`я', 'Surname' => 'Прізвище', 'Role' => 'Роль', 'You can change your role below :' => 'Ви можете змінити вашу роль на сайте нижче :', 'User edit' => 'Редагування користувачів', 'Registration' => 'Реєстрація', 'Hello Guest' => 'Привіт Гість', 'Enter login and password for authorization' => 'Введіть логін і пароль для авторизації', 'Login' => 'Логін', 'Password' => 'Пароль', 'Language' => 'Мова', 'Search' => 'Пошук', 'Sign out' => 'Вихід з облікового запису', 'Back' => 'Повернутися назад', 'User table' => 'Таблиця користувачів', 'Edit' => 'Редагувати', 'Delete' => 'Видалити', 'Add new user' => 'Додати нового користувача', 'Change user data' => 'Зміна даних про користувача', 'Account creation' => 'Створення облікового запису', 'Enter this data to search' => 'Введіть ці дані для пошуку', 'Searched user' => 'Шуканий користувач'],
        'en' => ['Hello' => 'Hello', 'You' => 'You', 'can' => 'can', 'change' => 'change', 'language' => 'language', 'below' => 'below', 'Personal cabinet' => 'Personal account', 'Control panel' => 'Control panel', 'Name' => 'Name', 'Surname' => 'Surname', 'Role' => 'Role', 'You can change your role below :' => 'You can change your role below :', 'User edit' => 'User edit', 'Registration' => 'Registration', 'Hello Guest' => 'Hello Guest', 'Enter login and password for authorization' => 'Enter login and password for authorization', 'Login' => 'Login', 'Password' => 'Password', 'Language' => 'Language', 'Search' => 'Search', 'Sign out' => 'Sign out', 'Back' => 'Back', 'User table' => 'User table', 'Edit' => 'Edit', 'Delete' => 'Delete', 'Add new user' => 'Add new user', 'Change user data' => 'Change user data', 'Account creation' => 'Account creation', 'Enter this data to search' => 'Enter this data to search', 'Searched user' => 'Searched user'],
        'it' => ['Hello' => 'Salve', 'You' => 'Puoi', 'can' => 'cambiare', 'change' => 'qui', 'language' => 'la lingua', 'below' => 'sotto', 'Personal cabinet' => 'Ufficio privato', 'Control panel' => 'Pannello di comando', 'Name' => 'Nome', 'Surname' => 'Cognome', 'Role' => 'Ruolo', 'You can change your role below :' => 'Puoi cambiare il tuo ruolo qui sotto :', 'User edit' => 'Modifica dellutente', 'Registration' => 'Registrazione', 'Hello Guest' => 'Ciao ospite', 'Enter login and password for authorization' => 'Immettere login e password per l`autorizzazione', 'Login' => 'Accesso', 'Password' => 'Parola d`ordine', 'Language' => 'Lingua', 'Search' => 'Ricerca', 'Sign out' => 'Disconnessione', 'Back' => 'Indietro', 'User table' => 'Tabella utente', 'Edit' => 'Modificare', 'Delete' => 'Elimina', 'Add new user' => 'Aggiungi un nuovo utente', 'Change user data' => 'Modifica i dati dell`utente', 'Account creation' => 'Creazione account', 'Enter last name to search' => 'Entrez ces données pour rechercher', 'Searched user' => 'Utente cercato']];
function auth()
{
    if (!isset($_SESSION['id'])) {
        return null;
    }
    $id=$_SESSION['id'];
    $servername = "localhost";
    $username = "root";
    $password = "";
    $conn = new mysqli($servername, $username, $password, users);
    $temp_user=mysqli_query($conn,"SELECT * FROM `users` WHERE `id` = '$id'");
    $temp_user = mysqli_fetch_assoc($temp_user);
    if($temp_user['role']=='admin'){
        return $user = new admin($temp_user);
    }
    if($temp_user['role']=='manager'){
        return $user = new manager($temp_user);
    }
    if($temp_user['role']=='client'){
        return $user = new client($temp_user);
    }
}
$user = auth();
if($_SESSION['id']){
if($user==NULL){
    header("Location : index.php");
}
}
$lang=$_SESSION['lang'];
?>