<?php
require_once("check.php");
if($user->isClient() || $user==NULL){
    header('Location: index.php');
}
else {
?>
<html>
<body>
<h1><?=$translate[$lang]['Account creation']?></h1>
<form action="users.php" method="POST">
    <?=$translate[$lang]['Name']?> :
    <br>
    <input name='name' type="text"/>
    <br>
    <?=$translate[$lang]['Surname']?> :
    <br>
    <input name='surname' type="text"/>
    <br>
    <?=$translate[$lang]['Login']?> :
    <br>
    <input name='login' type="text"/>
    <br>
    <?=$translate[$lang]['Password']?> :
    <br>
    <input name='password' type="password"/>
    <br>
    <input name='temp' type="hidden" value="1"/>
    <?=$translate[$lang]['Language']?> :
    <br>
    <select name="language">
        <option value="ru">ru</option>
        <option value="ua">ua</option>
        <option value="it">it</option>
        <option value="en">en</option>
    </select>
    <br>
    <?=$translate[$lang]['Role']?> :
    <br>
    <select name="role">
        <option value="admin">admin</option>
        <option value="manager">manager</option>
        <option value="client">client</option>
    </select>
    <br>
    <a href="users.php"><input type="submit" val='send!'/>
    </a>
</form>
</body>
</html>
<?php } ?>