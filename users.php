<?php
require_once("check.php");
    if($user->isAdmin() || $user->isManager()){
    if(isset($_POST['del'])){
    delete_users($conn,$_POST['del']);
    echo "User №".$_POST['del']." deleted successful";
}
if(isset($_POST['id'])){
    edit_users($conn, $_POST);
    echo "User №".$_POST['id']." updated successful";
}
if (isset($_POST['temp'])) {
    $a=0;
foreach ($users as $key => $user) {
    if ($_POST['login'] == $user['login']) {
        edit_users($conn, $_POST);
        $a=1;
    }
}
if($a==0) {
        add_users($conn, $_POST);
    }
}
?>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<table border="1">
    <caption><?=$translate[$lang]['User table']?></caption>
    <tr>
        <th>id</th>
        <th><?=$translate[$lang]['Name']?></th>
        <th><?=$translate[$lang]['Surname']?></th>
        <th><?=$translate[$lang]['Login']?></th>
        <th><?=$translate[$lang]['Language']?></th>
        <th><?=$translate[$lang]['Role']?></th>
    </tr>
    <?php
    foreach ($users as $key => $user1) {
        ?>
        <tr>
            <td><?php echo $user1['id'] ?></td>
            <td><?php echo $user1['name'] ?></td>
            <td><?php echo $user1['surname'] ?></td>
            <td><?php echo $user1['login'] ?></td>
            <td><?php echo $user1['language'] ?></td>
            <td><?php echo $user1['role'] ?></td>
            <?php if($user->isAdmin()){?><td><a href="edit.php?id=<?= $user1['id']?>">edit</a></td>
            <td><a href="users.php?del=<?=$user1['id']?>">delete</a></td>
        </tr>
        <?php
    }}
    ?>
</table>
<br>
<?php if($user->isAdmin()){?><a href="add_user.php?add=new"><?=$translate[$lang]['Add new user']?></a>
<br><?php }?>
<a href="<?=$user->role?>.php"><?=$translate[$lang]['Back']?></a>
</body>
</html>
<?php }
else{
    header("Location : client.php");
} ?>