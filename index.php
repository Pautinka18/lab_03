<?php
session_start();
session_destroy();
require_once ("check.php");
$lang='en';
if(isset($_POST['lang'])){
$lang=$_POST['lang'];
}
$_SESSION['lang']=$lang;
if (isset($_POST)) {
    $_POST['role'] = 'client';
    $a=0;
    foreach ($users as $key => $user) {
        if ($_POST['login'] == $user['login']) {
            $id = $user['id'];
            $_POST['role']=$user['role'];
            edit_users($conn, $_POST);
            $a=1;
            header("Location : index.php");
        }
    }
    if($a==0) {
        add_users($conn, $_POST);
    }
}
?>
<html>
<body>
<h1><?=$translate[$lang]['Hello Guest']?>!</h1>
<br>
<?=$translate[$lang]['Enter login and password for authorization']?> :
<form action="login.php" method="POST">
    <input name='names' type="text"/>
    <input name='second' type="password"/>
    <input type="submit" val='send!'/>
</form>
<a href="register.php"><?=$translate[$lang]['Registration']?></a>

<form action="index.php" method="POST">
    <select name="lang">
        <option value="ru">ru</option>
        <option value="ua">ua</option>
        <option value="it">it</option>
        <option value="en">en</option>
    </select>
    <input type="submit"/>
</form>
</body>
</html>